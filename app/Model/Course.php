<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    //
    public function student()
    {
        return $this->belongsToMany('App\Model\Student','student_course','course_id','student_id');
    }
}
