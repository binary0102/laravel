<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //
    public function course()
    {
        return $this->belongsToMany('App\Model\Course','student_course','student_id','course_id');
    }

}
