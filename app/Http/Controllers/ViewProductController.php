<?php

namespace App\Http\Controllers;

use App\Model\Course;
use Illuminate\Http\Request;
use App\Post;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Model\Student;
class ViewProductController extends Controller
{
    //
    public function getView()
    {
        $posts = Post::all();

        $posts->load('user');




        return view('frontend.index')->with('posts',$posts);

    }
    public function getPost($id)
    {
        $posts = Post::find($id);

        return view('frontend.page')->with('posts',$posts);
    }

}
