<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\VarDumper\Cloner\Data;
use Yajra\Datatables\Datatables;
use Collective\Html\FormBuilder;
use App\Model\Student;
use App\Model\Course;

class DataController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getStudent(Request $request)
    {
        return view('quanly.test');
    }


    public function dataStudent(Datatables $datatables)
    {

        $query = Student::with('course')
            ->select('id', 'name', 'phone')->orderBy('id', 'desc');

        return $datatables->eloquent($query)->addColumn('course', function (Student $post) {

            return $post->course->pluck('name')->implode('<br>');

        })->rawColumns(['course'])
            ->make(true);
    }

    public function getCourse(Request $request)
    {
        return view('quanly.course');
    }

    public function dataCourse(Datatables $datatables)
    {

        $query = Course::with('student')->select('id', 'name', 'tuition', 'information');

        return $datatables->eloquent($query)->addColumn('student', function (Course $course) {

            return $course->student->count();

        })->rawColumns(['student1'])
            ->make(true);
    }

    public function request()
    {

    }


}
