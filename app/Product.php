<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    protected $fillable = ['name', 'alias', 'price', 'intro', 'content', 'image','keywords','description','user_id','cate_id'];
    public $timestamps= false;

    public function categories()
    {
        return $this->belongsTo('App\Category');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function ProductImage()
    {
        return $this->belongsTo('App\ProductImage');
    }
}
