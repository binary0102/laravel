<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('posts', 'PostController');
Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');
Route::resource('permissions','PermissionController');
Route::resource('categorys','Admin\AdminCategoryController');
Route::resource('products','ProductsController');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('/','ViewProductController@getView');
Route::get('/post/{id}','ViewProductController@getPost');
Route::get('/about',function (){
    return view('frontend.about');
});
Route::get('/contact',function (){
    return view('frontend.contact');
});
Route::get('/test', function ()
{
    return view('quanly/index');
});
Route::get('/test1', function ()
{
    return view('quanly/test');
});

Route::get('test2', 'DataController@getStudent');
Route::get('data2', 'DataController@dataStudent');
Route::get('test3', 'DataController@getCourse');
Route::get('data3', 'DataController@dataCourse');