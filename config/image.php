<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',
        /*
    |--------------------------------------------------------------------------
    | Default Image Paths and Settings
    |--------------------------------------------------------------------------
    |
    |
    | We set the config here so that we can keep our controllers clean.
    | Configure each image type with an image path.
    |
    */
    'marketingImage' => [
    'destinationFolder'     => '/imgs/marketing/',
    'destinationThumbnail'  => '/imgs/marketing/thumbnails/',
    'thumbPrefix'           => 'thumb-',
    'imagePath'             => '/imgs/marketing/',
    'thumbnailPath'         => '/imgs/marketing/thumbnails/thumb-',
    'thumbHeight'           => 60,
    'thumbWidth'            => 60,
],


];
