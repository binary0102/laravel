@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Quản Lý Học Viên</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                    Quản Lý Học Viên

                </div>
                <div class="panel-body">
                    <table id="student-course-table" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tên Học Viên</th>
                            <th>Điện Thoại</th>
                            <th>Tên Khóa </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection


@section('script')
    <script src="vendor1/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor1/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor1/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <script>
        /*
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });*/
        $(function () {
            $('#student-course-table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{url('data2')}}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'phone', name: 'phone'},
                    {data: 'course',name: 'course.name'},
                ]
            });
        });

    </script>
@endsection

