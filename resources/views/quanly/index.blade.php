@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tables</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                    DataTables Advanced Tables

                </div>
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>

                            <th>Họ và tên</th>
                            <th>Số Điên Thoai</th>
                            <th>Email</th>
                            <th>Trạng Thái</th>
                            <th>Khóa Học</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="odd gradeX">
                            <td>Nguyễn Văn A</td>
                            <td>012382131823</td>
                            <td>asaaaa@gmail.com</td>
                            <td>Đặt Cọc </td>
                            <th>Nâng Cao</th>
                        </tr>
                        <tr class="odd gradeX">
                            <td>Nguyễn Văn B</td>
                            <td>012382131823</td>
                            <td>asaaaa@gmail.com</td>
                            <td>Miễn Phi </td>
                            <th>Cơ B</th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection


@section('script')
    <script src="vendor1/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor1/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor1/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
@endsection

