@extends('layouts.view')
@section('heading')
    <div class="post-heading">
        <h1>{{$posts['title']}}</h1>
        <h2 class="subheading">{{$posts['subheading']}}</h2>
        <span class="meta">Posted by
                <a href="#">{{$posts->user->name}}</a>
               on {{$posts['created_at']}}</span>
    </div>
@endsection
@section('contain')
    <div class="row">

                {!! $posts['body'] !!}

    </div>

@endsection

