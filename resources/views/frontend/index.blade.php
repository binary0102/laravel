@extends('layouts.view')
@section('heading')
    <div class="site-heading">
        <h1>Computer</h1>
        <span class="subheading">Binary </span>
    </div>
@endsection
@section('contain')
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                @foreach($posts as $post)
                    <div class="post-preview">
                        <a href="{{url("post/".$post['id'])}}">
                            <h2 class="post-title">
                               {{$post['title']}}
                            </h2>
                            <h3 class="post-subtitle">
                                {{$post['subheading']}}
                            </h3>
                        </a>
                        <p class="post-meta">Posted by
                            <a href="#">{{$post->user->name}}</a>
                            on {{$post['created_at']}}</p>
                    </div>
                    <hr>
                    @endforeach
                    <div class="clearfix">
                        <a class="btn btn-primary float-right" href="#">Older Posts →</a>
                    </div>
            </div>
        </div>

@endsection
