{{-- \resources\views\users\index.blade.php --}}
@extends('layouts.app')

@section('title', '| Users')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">
        <h1><i class="fa fa-users"></i> Products <a href="{{ route('products.index') }}" class="btn btn-default pull-right">Roles</a>
            <a href="{{ route('permissions.index') }}" class="btn btn-default pull-right">Permissions</a></h1>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">

                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Info</th>
                    <th>Context</th>
                    <th>price</th>
                    <th>Image</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>

                @foreach ($products as $product)
                    <tr>

                        <td>{{ $product->id    }}</td>
                        <td>{{ $product->name  }}</td>
                        <td>{{ $product->intro }}</td>
                        <td>{{ $product->content  }}</td>
                        <td>{{ $product->price }}</td>
                        <td><img src="{{$product->image}}" alt=""></td>

                        <td>
                            <a href="{{ route('users.edit', $product->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                            {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $product->id] ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>

        <a href="{{ route('products.create') }}" class="btn btn-success">Add User</a>

    </div>

@endsection