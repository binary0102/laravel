{{-- \resources\views\users\index.blade.php --}}
@extends('layouts.app')

@section('title', '| Users')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{ Form::open(array('url' => 'products','enctype' =>'multipart/form-data')) }}
    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('price', 'Price') }}
        {{ Form::text('price', null, array('class' => 'form-control')) }}

    </div>
    <div class="form-group">
        {{ Form::label('name', 'Category') }}
        <select class="form-control">
            @foreach($categories as $category)
                <option>{{$category->name}}</option>
            @endforeach
        </select>



    </div>
    <div class="form-group">
        {{ Form::label('Content', 'Name') }}
        <textarea name="into" cols="30" rows="10" class="form-control"></textarea>
    </div>
    <div class="form-group">
        {{ Form::label('image', 'Image') }}
        <br>
        {{ Form::file('image') }}
    </div>

    {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}


@endsection