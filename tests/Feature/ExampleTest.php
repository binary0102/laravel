<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Post;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_user_relation()
    {
        $m = new Post();
        $relation = $m->user();
        $this->assertEquals('posts', $m->getTable());
        $this->assertEquals('id',    $m->getKeyName());
;    }
}
